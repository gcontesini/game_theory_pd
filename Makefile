
TEXPROJECT=main
BACKUP_FILE=backup_tex_$(shell date +%F).tar.gz 
TEXFILE=$(TEXPROJECT).tex
BIBFILE=bibdatabase.bib
LATEX=pdflatex
LANG=en

all: $(TEXFILE)
	latexmk -pdf -pdflatex='$(LATEX) -interactive=nostopmode' $(TEXFILE)

clean:
	rm -rf *.pdf *.aux *.toc *.log *.blg *.bbl *.fls *.latexmake *.fdb_latexmk

backup:
	tar -czvf old/$(BACKUP_FILE) *.tex images/*.eps

count:
	pdftotext $(TEXPROJECT).pdf - | wc -w

aspell:
	aspell -t -c $(TEXPROJECT).tex --encoding=utf-8 --lang=$(LANG)
